<?php

namespace App\Models;

class Post extends CustomModel
{
    public $timestamps = true;
    protected $guarded = [];
    protected $table = 'posts';
    protected $primaryKey = 'id';
    protected $checkbox = [
        'is_private',
        'is_published',
    ];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'title' => 'string',
		'meta_title' => 'string',
		'meta_description' => 'text',
		'body' => 'text',
		'category_id' => 'integer',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
		'deleted_at' => 'datetime',
	];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'tags_posts');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}