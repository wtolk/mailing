<?php

namespace App\Models;

class Tag extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'tags';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'title' => 'string',
	];

    public function post()
    {
        return $this->belongsToMany('App\Models\Post');
    }

}