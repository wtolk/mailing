<?php

namespace App\Models;

class Category extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'categories';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'title' => 'string',
	];

}