<?php

namespace App\Helpers;

use App\Models\Category;
use App\Models\NavigationItem;
use App\Models\Tag;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigCustomExtensions extends AbstractExtension
{
    public function __construct($container) {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new TwigFunction('getNavigation', array($this, 'getNavigation')),
            new TwigFunction('getCategories', array($this, 'getCategories')),
            new TwigFunction('getTags', array($this, 'getTags')),
            new TwigFunction('parsedown', array($this, 'parsedown')),
        );
    }

    public function parsedown($text)
    {
        $parsedown = new \Parsedown();
        return $parsedown->text($text);
    }

    public function getCategories()
    {
        return Category::all()->toArray();
    }

    public function getTags()
    {
        return Tag::all()->toArray();
    }


    public function getNavigation($menu_id)
    {
        $user = $this->container['sentinel']->check();
        $items = NavigationItem::where('navigation_id', $menu_id)->orderBy('position')->where('parent_id', null)->with('children')->get()->toArray();

        foreach ($items as &$item)
        {
            $this->hasAccess($user, $item);

            if (count($item['children']) > 0)
            {
                foreach ($item['children'] as &$sub_item)
                {
                    $this->hasAccess($user, $sub_item);
                }

            }

        }

        unset($item, $sub_item);


        $len = 0;
        $active_key = 0;
        $sub_active_key = null;
        foreach($items as $key=>$item)
        {
            $path = $this->container->router->pathFor($item['path']);
            $pos = strripos($_SERVER['REQUEST_URI'], $path);

            if ($pos || $pos === 0)
            {
                if (strlen($path) > $len)
                {
                    $len = strlen($path);
                    $active_key = $key;
                }
            }

            if (count($item['children']) > 0)
            {
                foreach ($item['children'] as $sub_key=>$sub_item)
                {
                    $path = $this->container->router->pathFor($sub_item['path']);
                    $pos = strripos($_SERVER['REQUEST_URI'], $path);

                    if ($pos || $pos === 0)
                    {
                        if (strlen($path) > $len)
                        {
                            $len = strlen($path);
                            $sub_active_key = $sub_key;
                            $sub_active_parent_key = $key;
                        }
                    }
                }

            }
        }

        if (!is_null($sub_active_key))
        {
            $items[$sub_active_parent_key]['children'][$sub_active_key]['class'] = 'active-link';
        }else{
            $items[$active_key]['class'] = 'active-link';
        }

        return $items;

    }

    public function hasAccess($user, $item)
    {
        $permissions = json_decode($item['permissions']);

        $permissions_arr = [];
        foreach ($permissions as $permission_name=>$value)
        {
            if($value == 'true')
            {
                array_push($permissions_arr, $permission_name);
            }
        }

        if (!$user->hasAccess($permissions_arr)) {
            unset($item);
        }
    }
}