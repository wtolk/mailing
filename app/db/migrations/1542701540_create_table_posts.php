<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_posts_1542701540 {
    public function up() {
        Capsule::schema()->create('posts', function($table) {
            $table->increments('id');
			$table->string('title')->nullable();
			$table->string('meta_title')->nullable();
			$table->text('meta_description')->nullable();
			$table->text('body')->nullable();
			$table->boolean('is_published')->nullable();
			$table->boolean('is_private')->nullable();
			$table->integer('category_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->datetime('created_at')->nullable();
			$table->datetime('updated_at')->nullable();
			$table->datetime('deleted_at')->nullable();
			
        });

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
