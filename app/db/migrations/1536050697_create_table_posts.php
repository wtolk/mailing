<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_posts_1536050697 {
    public function up() {
        Capsule::schema()->create('posts', function($table) {
            $table->increments('id');
			$table->string('title');
			$table->string('meta_title')->nullable();
			$table->text('meta_description')->nullable();
			$table->text('body')->nullable();
			$table->boolean('is_published')->nullable();
			$table->boolean('is_private')->nullable();
			$table->integer('category_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->datetime('created_at')->nullable();
			$table->datetime('updated_at')->nullable();
			$table->datetime('deleted_at')->nullable();
        });

        Capsule::table('posts')->insert([
			'id' => '1',
			'title' => 'Пост 1',
			'meta_title' => '',
			'meta_description' => 'Описание',
			'body' => '* Текст',
			'is_published' => '1',
			'is_private' => '1',
			'category_id' => '1',
			'user_id' => '0',
			'created_at' => '2018-08-30 16:59:06',
			'updated_at' => '2018-09-03 15:04:06',
			'deleted_at' => '0000-00-00 00:00:00',
		]);

		Capsule::table('posts')->insert([
			'id' => '6',
			'title' => 'sadv',
			'meta_title' => '',
			'meta_description' => '',
			'body' => 'sdav',
			'is_published' => '1',
			'is_private' => '0',
			'category_id' => '1',
			'user_id' => '0',
			'created_at' => '2018-08-31 10:46:11',
			'updated_at' => '2018-08-31 16:36:32',
			'deleted_at' => '0000-00-00 00:00:00',
		]);

		Capsule::table('posts')->insert([
			'id' => '7',
			'title' => 'sac',
			'meta_title' => '',
			'meta_description' => '',
			'body' => '',
			'is_published' => '1',
			'is_private' => '0',
			'category_id' => '0',
			'user_id' => '0',
			'created_at' => '2018-08-31 10:47:12',
			'updated_at' => '2018-08-31 16:36:53',
			'deleted_at' => '0000-00-00 00:00:00',
		]);

		Capsule::table('posts')->insert([
			'id' => '11',
			'title' => 'asc',
			'meta_title' => 'asc',
			'meta_description' => 'asc',
			'body' => 'sac',
			'is_published' => '1',
			'is_private' => '1',
			'category_id' => '1',
			'user_id' => '1',
			'created_at' => '2018-08-31 10:48:59',
			'updated_at' => '2018-08-31 10:48:59',
			'deleted_at' => '0000-00-00 00:00:00',
		]);

		
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
