<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_role_users_1536051743 {
    public function up() {
        Capsule::schema()->create('role_users', function($table) {
            $table->integer('user_id');
			$table->integer('role_id');
			$table->datetime('created_at')->nullable();
			$table->datetime('updated_at')->nullable();
			
        });

        Capsule::table('role_users')->insert([
			'user_id' => '1',
			'role_id' => '1',
			'created_at' => '2018-09-03 14:16:25',
			'updated_at' => '2018-09-03 14:16:25',
		]);

		
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
