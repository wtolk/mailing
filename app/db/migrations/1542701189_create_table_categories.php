<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_categories_1542701189 {
    public function up() {
        Capsule::schema()->create('categories', function($table) {
            $table->increments('id')->nullable();
			$table->string('title')->nullable();
			
        });
    }
}
