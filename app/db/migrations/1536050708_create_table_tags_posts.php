<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_tags_posts_1536050708 {
    public function up() {
        Capsule::schema()->create('tags_posts', function($table) {
            $table->increments('id');
			$table->integer('tag_id');
			$table->integer('post_id');
			
        });

        Capsule::table('tags_posts')->insert([
			'id' => '2',
			'tag_id' => '2',
			'post_id' => '1',
		]);

		Capsule::table('tags_posts')->insert([
			'id' => '6',
			'tag_id' => '1',
			'post_id' => '11',
		]);

		Capsule::table('tags_posts')->insert([
			'id' => '7',
			'tag_id' => '2',
			'post_id' => '11',
		]);

		Capsule::table('tags_posts')->insert([
			'id' => '8',
			'tag_id' => '3',
			'post_id' => '11',
		]);

		Capsule::table('tags_posts')->insert([
			'id' => '9',
			'tag_id' => '2',
			'post_id' => '6',
		]);

		Capsule::table('tags_posts')->insert([
			'id' => '10',
			'tag_id' => '1',
			'post_id' => '6',
		]);

		Capsule::table('tags_posts')->insert([
			'id' => '11',
			'tag_id' => '3',
			'post_id' => '7',
		]);

		
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
