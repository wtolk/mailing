<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_tags_posts_1542701666 {
    public function up() {
        Capsule::schema()->create('tags_posts', function($table) {
            $table->increments('id');
			$table->integer('tag_id')->nullable();
			$table->integer('post_id')->nullable();
        });
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
