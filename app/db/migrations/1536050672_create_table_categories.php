<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_categories_1536050672 {
    public function up() {
        Capsule::schema()->create('categories', function($table) {
            $table->increments('id');
			$table->string('title');
        });

        Capsule::table('categories')->insert([
			'id' => '1',
			'title' => 'тестовая',
		]);

		
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
