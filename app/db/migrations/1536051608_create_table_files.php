<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_files_1536051608 {
    public function up() {
        Capsule::schema()->create('files', function($table) {
            $table->increments('id');
			$table->string('filename');
			$table->string('md5_filename');
			$table->string('path');
			$table->string('extension');
			$table->string('size');
			$table->datetime('created_at')->nullable();
			
        });

        Capsule::table('files')->insert([
			'id' => '1',
			'filename' => 'screen.png',
			'md5_filename' => 'bac1b1f2782cd5427ed194371ed9f8cb.png',
			'path' => '/files/ba/c1/b1f2782cd5427ed194371ed9f8cb.png',
			'extension' => 'png',
			'size' => '18260',
			'created_at' => '2018-08-31 11:07:25',
		]);

		Capsule::table('files')->insert([
			'id' => '2',
			'filename' => 'screen.png',
			'md5_filename' => '850ef8a8f6d37ea2ad13e94fdc8a4f1e.png',
			'path' => '/files/85/0e/f8a8f6d37ea2ad13e94fdc8a4f1e.png',
			'extension' => 'png',
			'size' => '18260',
			'created_at' => '2018-08-31 11:08:16',
		]);

		Capsule::table('files')->insert([
			'id' => '3',
			'filename' => 'screen.png',
			'md5_filename' => '17fa14ec79f45a8c09fe01fd5015b8b1.png',
			'path' => '/files/17/fa/14ec79f45a8c09fe01fd5015b8b1.png',
			'extension' => 'png',
			'size' => '18260',
			'created_at' => '2018-08-31 11:09:12',
		]);

		Capsule::table('files')->insert([
			'id' => '4',
			'filename' => 'screen.png',
			'md5_filename' => '2f567ca351c46c44d4e53b8d7dc8b3e6.png',
			'path' => '/files/2f/56/7ca351c46c44d4e53b8d7dc8b3e6.png',
			'extension' => 'png',
			'size' => '18260',
			'created_at' => '2018-08-31 11:09:23',
		]);

		Capsule::table('files')->insert([
			'id' => '5',
			'filename' => 'screen.png',
			'md5_filename' => 'b1c39bb028beec36bd44573808ab5824.png',
			'path' => '/files/b1/c3/9bb028beec36bd44573808ab5824.png',
			'extension' => 'png',
			'size' => '18260',
			'created_at' => '2018-08-31 11:09:46',
		]);

		Capsule::table('files')->insert([
			'id' => '6',
			'filename' => 'screen.png',
			'md5_filename' => '5959c726ece769b7d736a3d91612bc59.png',
			'path' => '/files/59/59/c726ece769b7d736a3d91612bc59.png',
			'extension' => 'png',
			'size' => '18260',
			'created_at' => '2018-08-31 11:09:57',
		]);

		Capsule::table('files')->insert([
			'id' => '7',
			'filename' => 'screen.png',
			'md5_filename' => '89a4c048cc7448da3039a2e8aa5a5799.png',
			'path' => '/files/89/a4/c048cc7448da3039a2e8aa5a5799.png',
			'extension' => 'png',
			'size' => '18260',
			'created_at' => '2018-08-31 11:10:07',
		]);

		Capsule::table('files')->insert([
			'id' => '8',
			'filename' => 'plus.png',
			'md5_filename' => '556147d599a5f74a5f740e2dc75d09b1.png',
			'path' => '/files/55/61/47d599a5f74a5f740e2dc75d09b1.png',
			'extension' => 'png',
			'size' => '294',
			'created_at' => '2018-08-31 11:16:16',
		]);

		Capsule::table('files')->insert([
			'id' => '9',
			'filename' => 'screen.png',
			'md5_filename' => 'a9101796e416e6f6397e908e8725b569.png',
			'path' => '/files/a9/10/1796e416e6f6397e908e8725b569.png',
			'extension' => 'png',
			'size' => '18260',
			'created_at' => '2018-08-31 11:16:32',
		]);

		Capsule::table('files')->insert([
			'id' => '10',
			'filename' => '4.jpg',
			'md5_filename' => '9846c939d318ccaae9e1a2a326808ec4.jpg',
			'path' => '/files/98/46/c939d318ccaae9e1a2a326808ec4.jpg',
			'extension' => 'jpg',
			'size' => '214119',
			'created_at' => '2018-09-03 14:16:25',
		]);

		
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
