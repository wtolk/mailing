<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_throttle_1536051760 {
    public function up() {
        Capsule::schema()->create('throttle', function($table) {
            $table->increments('id');
			$table->integer('user_id');
			$table->string('type');
			$table->string('ip');
			$table->datetime('created_at')->nullable();
			$table->datetime('updated_at')->nullable();
			
        });

        
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
