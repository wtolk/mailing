<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_navigation_1536051690 {
    public function up() {
        Capsule::schema()->create('navigation', function($table) {
            $table->increments('id');
			$table->string('title');
			
        });

        Capsule::table('navigation')->insert([
			'id' => '1',
			'title' => 'Административное',
		]);

		Capsule::table('navigation')->insert([
			'id' => '2',
			'title' => 'Пользовательское',
		]);

		
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
