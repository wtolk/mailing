<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_navigation_items_1536051695 {
    public function up() {
        Capsule::schema()->create('navigation_items', function($table) {
            $table->increments('id');
			$table->integer('navigation_id');
			$table->string('title');
			$table->text('path');
			$table->float('position');
			$table->integer('parent_id');
			$table->text('permissions');
        });

        Capsule::table('navigation_items')->insert([
			'id' => '1',
			'navigation_id' => '1',
			'title' => 'Пользователи',
			'path' => 'user.showAdminUserList',
			'position' => '1',
			'parent_id' => '',
			'permissions' => '',
		]);

		Capsule::table('navigation_items')->insert([
			'id' => '2',
			'navigation_id' => '1',
			'title' => 'Навигация',
			'path' => 'navigation.showAdminNavigationList',
			'position' => '8',
			'parent_id' => '',
			'permissions' => '',
		]);

		Capsule::table('navigation_items')->insert([
			'id' => '3',
			'navigation_id' => '1',
			'title' => 'Посты',
			'path' => 'post.showAdminPostList',
			'position' => '9',
			'parent_id' => '',
			'permissions' => '{"user_view":"false","user_create":"false","user_update":"false","user_delete":"false","filials_view":"false","filials_create":"false","filials_update":"false","filials_delete":"false","programms_view":"false","programms_create":"false","programms_update":"false","programms_delete":"false","price-types_view":"false","price-types_create":"false","price-types_update":"false","price-types_delete":"false","roles_view":"false","reports_view":"false","produce_view":"false","produce_create":"false","produce_update":"false","produce_delete":"false","menu_view":"false","menu_create":"false","menu_update":"false","menu_delete":"false","orders_view":"false","orders_create":"false","orders_update":"false","orders_delete":"false"}',
		]);

		Capsule::table('navigation_items')->insert([
			'id' => '4',
			'navigation_id' => '1',
			'title' => 'Категории',
			'path' => 'category.showAdminCategoryList',
			'position' => '10',
			'parent_id' => '',
			'permissions' => '{"user_view":"false","user_create":"false","user_update":"false","user_delete":"false","filials_view":"false","filials_create":"false","filials_update":"false","filials_delete":"false","programms_view":"false","programms_create":"false","programms_update":"false","programms_delete":"false","price-types_view":"false","price-types_create":"false","price-types_update":"false","price-types_delete":"false","roles_view":"false","reports_view":"false","produce_view":"false","produce_create":"false","produce_update":"false","produce_delete":"false","menu_view":"false","menu_create":"false","menu_update":"false","menu_delete":"false","orders_view":"false","orders_create":"false","orders_update":"false","orders_delete":"false"}',
		]);

		
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
