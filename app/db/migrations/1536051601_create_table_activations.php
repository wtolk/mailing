<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_activations_1536051601 {
    public function up() {
        Capsule::schema()->create('activations', function($table) {
            $table->increments('id');
			$table->integer('user_id');
			$table->string('code');
			$table->boolean('completed');
			$table->datetime('completed_at')->nullable();
			$table->datetime('created_at')->nullable();
			$table->datetime('updated_at')->nullable();
			
        });

        Capsule::table('activations')->insert([
			'id' => '1',
			'user_id' => '1',
			'code' => 'L1MfTNdG5l6RkO0bAIAUzmPZKkUDITYx',
			'completed' => '1',
			'completed_at' => '2018-08-30 16:24:05',
			'created_at' => '2018-08-30 16:24:05',
			'updated_at' => '2018-08-30 16:24:05',
		]);

		
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
