<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_persistences_1536051719 {
    public function up() {
        Capsule::schema()->create('persistences', function($table) {
            $table->increments('id');
			$table->integer('user_id');
			$table->string('code');
			$table->datetime('created_at')->nullable();
			$table->datetime('updated_at')->nullable();
			
        });

        Capsule::table('persistences')->insert([
			'id' => '1',
			'user_id' => '1',
			'code' => 'UsQ79BCvFdfTwY6L6IOiWDLMxLKxPu9l',
			'created_at' => '2018-08-30 16:24:10',
			'updated_at' => '2018-08-30 16:24:10',
		]);

		Capsule::table('persistences')->insert([
			'id' => '2',
			'user_id' => '1',
			'code' => 'wKuGt9sCfJBRjEs74jXarUlJpDigrF3i',
			'created_at' => '2018-08-31 10:27:31',
			'updated_at' => '2018-08-31 10:27:31',
		]);

		Capsule::table('persistences')->insert([
			'id' => '3',
			'user_id' => '1',
			'code' => 'eXGohyoZpeBhjxGoPpdThLchYRMiSkoe',
			'created_at' => '2018-08-31 13:21:13',
			'updated_at' => '2018-08-31 13:21:13',
		]);

		Capsule::table('persistences')->insert([
			'id' => '8',
			'user_id' => '1',
			'code' => 'IlDZ9gJCOR7ADGZGG5GQojVqxXKdPjMm',
			'created_at' => '2018-09-03 14:15:22',
			'updated_at' => '2018-09-03 14:15:22',
		]);

		
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
