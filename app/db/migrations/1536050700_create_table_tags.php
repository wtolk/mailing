<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_tags_1536050700 {
    public function up() {
        Capsule::schema()->create('tags', function($table) {
            $table->increments('id');
			$table->string('title');
			
        });

        Capsule::table('tags')->insert([
			'id' => '1',
			'title' => 'sdv',
		]);

		Capsule::table('tags')->insert([
			'id' => '2',
			'title' => 'тег',
		]);

		Capsule::table('tags')->insert([
			'id' => '3',
			'title' => 'ппп',
		]);

		
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
