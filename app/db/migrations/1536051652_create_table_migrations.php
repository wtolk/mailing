<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_migrations_1536051652 {
    public function up() {
        Capsule::schema()->create('migrations', function($table) {
            $table->increments('id');
			$table->string('filename');
			$table->string('class');
			$table->integer('time');
        });

        
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
