<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_reminders_1536051729 {
    public function up() {
        Capsule::schema()->create('reminders', function($table) {
            $table->increments('id');
			$table->integer('user_id');
			$table->string('code');
			$table->boolean('completed');
			$table->datetime('completed_at')->nullable();
			$table->datetime('created_at')->nullable();
			$table->datetime('updated_at')->nullable();
			
        });

        
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
