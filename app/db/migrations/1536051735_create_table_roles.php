<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_roles_1536051735 {
    public function up() {
        Capsule::schema()->create('roles', function($table) {
            $table->increments('id');
			$table->string('slug');
			$table->string('name');
			$table->text('permissions');
			
        });

        Capsule::table('roles')->insert([
			'id' => '1',
			'slug' => 'TestRole',
			'name' => 'Тестовая',
			'permissions' => '{"user_view":true,"user_create":true,"user_update":true,"user_delete":true,"roles_view":true,"posts_view":true,"posts_create":true,"posts_update":true,"posts_delete":true,"categorys_view":true,"categorys_create":true,"categorys_update":true,"categorys_delete":true}',
		]);

		
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
