<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_users_1536051751 {
    public function up() {
        Capsule::schema()->create('users', function($table) {
            $table->increments('id');
			$table->string('email')->nullable();
			$table->string('password');
			$table->text('permissions')->nullable();
			$table->datetime('last_login')->nullable();
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->integer('photo_id')->nullable();
			$table->datetime('created_at')->nullable();
			$table->datetime('updated_at')->nullable();
			
        });

        Capsule::table('users')->insert([
			'id' => '1',
			'email' => 'test',
			'password' => '$2y$10$VhoK8Vlj0QD/EE4BvwIK1OtUXMRbOY84RuKFe7Lqc6PbacJISmxwC',
			'permissions' => '',
			'last_login' => '2018-09-03 14:24:00',
			'first_name' => 'Тестовый',
			'last_name' => 'Пользователь',
			'photo_id' => '10',
			'created_at' => '2018-08-30 16:24:05',
			'updated_at' => '2018-08-30 16:24:05',
		]);

		
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
