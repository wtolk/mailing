<?php

use App\Controllers\TagController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Controllers\UserController;
use App\Controllers\RoleController;
use App\Middleware\Registred;
use App\Middleware\Access;
use App\Controllers\NavigationController;
use App\Controllers\NavigationItemController;
use App\Controllers\PostController;
use App\Controllers\CategoryController;
//delimiter//

$app->get('/login', UserController::class.':showLoginPage')->setName('user.showLoginPage');
$app->post('/api/login', UserController::class.':doLogin')->setName('user.doLogin');
$app->get('/api/logout', UserController::class.':doLogout')->setName('user.doLogout');

$app->group('/api', function() use($app, $c) {

    $app->post('/users', UserController::class . ':createUser')->setName('user.createUser')->add(new Access($c, ['user_create']));
    $app->post('/users/{id}', UserController::class . ':updateUser')->setName('user.updateUser')->add(new Access($c, ['user_update']));
    $app->post('/users/{id}/delete', UserController::class . ':deleteUser')->setName('user.deleteUser')->add(new Access($c, ['user_delete']));

    $app->post('/roles', RoleController::class . ':createRole')->setName('role.createRole')->add(new Access($c, ['roles_view']));
    $app->post('/roles/get_role', RoleController::class . ':getRole')->setName('role.getRole')->add(new Access($c, ['roles_view']));
    $app->post('/roles/create', RoleController::class . ':createUserRoles')->setName('role.createRole')->add(new Access($c, ['roles_view']));

    $app->post('/navigation/items/save', NavigationItemController::class.':saveNavigationItems')->setName('navigation.saveNavigationItems');
    $app->post('/navigation/get-item-permissions', NavigationItemController::class.':getItemPermissions')->setName('navigation.getItemPermissions');

    $app->post('/navigation', NavigationController::class.':createNavigation')->setName('navigation.createNavigation');
    $app->post('/navigation/{id}', NavigationController::class.':updateNavigation')->setName('navigation.updateNavigation');
    $app->get('/navigation/{id}/delete', NavigationController::class.':deleteNavigation')->setName('navigation.deleteNavigation');

    $app->post('/navigation/{id}/item', NavigationItemController::class.':createNavigationItem')->setName('navigation.createNavigationItem');
    $app->post('/navigation/{id}/item/{item_id}', NavigationItemController::class.':updateNavigationItem')->setName('navigation.updateNavigationItem');
    $app->get('/navigation/{id}/item/{item_id}/delete', NavigationItemController::class.':deleteNavigationItem')->setName('navigation.deleteNavigationItem');

    $app->post('/posts', PostController::class.':createPost')->setName('post.createPost')->add(new Access($c, ['posts_create']));
    $app->post('/posts/{id}', PostController::class.':updatePost')->setName('post.updatePost')->add(new Access($c, ['posts_update']));
    $app->get('/posts/{id}/delete', PostController::class.':deletePost')->setName('post.deletePost')->add(new Access($c, ['posts_delete']));

    $app->post('/tags', TagController::class.':createTag')->setName('tag.createTag')->add(new Access($c, ['posts_create']));

    $app->post('/categorys', CategoryController::class.':createCategory')->setName('category.createCategory')->add(new Access($c, ['categorys_create']));
    $app->post('/categorys/{id}', CategoryController::class.':updateCategory')->setName('category.updateCategory')->add(new Access($c, ['categorys_update']));
    $app->get('/categorys/{id}/delete', CategoryController::class.':deleteCategory')->setName('category.deleteCategory')->add(new Access($c, ['categorys_delete']));

})->add(new Registred($container));
