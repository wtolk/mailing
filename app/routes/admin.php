<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Controllers\TestController;
use App\Controllers\UserController;
use App\Controllers\RoleController;
use App\Middleware\Access;
use App\Middleware\Registred;
use App\Controllers\NavigationController;
use App\Controllers\NavigationItemController;
use App\Controllers\PostController;
use App\Controllers\CategoryController;
//delimiter//

$c = $app->getContainer();

$app->get('/message/no-access', UserController::class.':noAccess')->setName('error.noAccess');

#$app->get('/', TestController::class.':index')->setName('home');
$app->get('/upload', TestController::class.':upload');
#$app->get('/test', TestController::class.':test');

$app->group('/admin', function() use($app, $c) {

    $app->get('/users', UserController::class . ':showAdminUserList')->setName("user.showAdminUserList")->add(new Access($c, ['user_view']));
    $app->get('/users/add', UserController::class . ':showAdminUserAdd')->setName("user.showAdminUserAdd")->add(new Access($c, ['user_view', 'user_create']));
    $app->get('/users/{id}', UserController::class . ':showAdminUserEdit')->setName('user.showAdminUserEdit')->add(new Access($c, ['user_view', 'user_update']));

    $app->get('/roles', RoleController::class . ':showAdminRoleList')->setName('role.showAdminRoleList')->add(new Access($c, ['roles_view']));

    $app->get('/navigation', NavigationController::class . ':showAdminNavigationList')->setName('navigation.showAdminNavigationList');
    $app->get('/navigation/add', NavigationController::class . ':showAdminNavigationAdd')->setName('navigation.showAdminNavigationAdd');
    $app->get('/navigation/{id}', NavigationController::class . ':showAdminNavigationEdit')->setName('navigation.showAdminNavigationEdit');

    $app->get('/navigation/{id}/items', NavigationItemController::class.':showadminNavigationItemList')->setName('navigation.showAdminNavigationItemList');
    $app->get('/navigation/{id}/items/add', NavigationItemController::class.':showAdminNavigationItemAdd')->setName('navigation.showAdminNavigationItemAdd');
    $app->get('/navigation/{id}/items/{item_id}', NavigationItemController::class.':showAdminNavigationItemEdit')->setName('navigation.showAdminNavigationItemEdit');

    $app->get('/posts', PostController::class . ':showAdminPostList')->setName('post.showAdminPostList')->add(new Access($c, ['posts_view']));
    $app->get('/posts/add', PostController::class . ':showAdminPostAdd')->setName('post.showAdminPostAdd')->add(new Access($c, ['posts_create']));
    $app->get('/posts/{id}', PostController::class . ':showAdminPostEdit')->setName('post.showAdminPostEdit')->add(new Access($c, ['posts_update']));

})->add(new Registred($container));

$app->group('/admin', function() use($app, $c) {

    $app->get('/categorys', CategoryController::class . ':showAdminCategoryList')->setName('category.showAdminCategoryList')->add(new Access($c, ['categorys_view']));
    $app->get('/categorys/add', CategoryController::class . ':showAdminCategoryAdd')->setName('category.showAdminCategoryAdd')->add(new Access($c, ['categorys_create']));
    $app->get('/categorys/{id}', CategoryController::class . ':showAdminCategoryEdit')->setName('category.showAdminCategoryEdit')->add(new Access($c, ['categorys_update']));

})->add(new Registred($container));
