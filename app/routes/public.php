<?php

use App\Controllers\CategoryController;
use App\Controllers\PostController;

$app->get('/posts', PostController::class.':showPublicListPosts');
$app->get('/', PostController::class.':showPublicListPosts');
$app->get('/category/{id}', PostController::class.':showPublicListPostsFromCategory');
$app->get('/tag/{id}', PostController::class.':showPublicListPostsFromTag');
$app->get('/post/{id}', PostController::class.':showPublicPost');