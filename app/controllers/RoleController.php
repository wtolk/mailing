<?php

namespace App\Controllers;

use App\Models\Role;

class RoleController extends Controller
{
    public function showAdminRoleList($request, $response, $args)
    {
        $roles = Role::all();
        $this->view = 'admin/users/user-roles.twig';
        $this->twig_vars['roles'] = $roles;
        return $this->render();
    }


    public function getRole($request, $response, $args) {
        $id = $request->getParams()['id'];
        echo Role::find($id);
    }

    public function createUserRoles($request, $response, $args) {
        $data = $request->getParams();

        $role = [];
        if (isset($data['id'])) {
            $role['id'] = $data['id'];
            unset($data['id']);
        }

        $role['name'] = $data['name'];
        $role['slug'] = $data['slug'];

        unset($data['name']);
        unset($data['slug']);

        foreach ($data as $key => $value) {
            $role['permissions'][$key] = $value == 'true' ? true : false;
        }

        $role['permissions'] = json_encode($role['permissions']);

        if ($role['id']) {
            $r = Role::find($role['id']);
            $r->update($role);
        } else {
            unset($role['id']);
            Role::create($role);
        }

        return $response->withStatus(301)->withHeader('Location', '/admin/roles');
    }

}