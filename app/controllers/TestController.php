<?php

namespace App\Controllers;
use App\Helpers\FlockBot;
use App\Models\Role;
use Illuminate\Pagination\Paginator;

class TestController extends Controller
{
    public function index($request, $response, $args)
    {
        //$flockBot = new FlockBot();
        //$flockBot->sendMessage(FlockBot::$OUR_CHAT_ID, 'Проверка');
        $this->view = 'admin/test/test.twig';
        $this->render();
    }

    public function upload($request, $response, $args)
    {
        if(!empty($_FILES['file']) && $_FILES['file']['size'] > 0)
        {
            $file = $this->_uploadFiles('file')[0];
        }
    }

    //Создает пользователя test с паролем test
    public function test($request, $response, $args)
    {
        $role = Role::create(['name'=>'Тестовая','slug'=>'TestRole', 'permissions'=>'{"user_view":true,"user_create":true,"user_update":true,"user_delete":true, "roles_view":true}']);

        $credentials = [
            'email'    => 'test',
            'password' => 'test',
            'first_name' => 'Тестовый',
            'last_name' => 'Пользователь'
        ];

        $user = $this->ci['sentinel']->registerAndActivate($credentials);

        $role->users()->attach($user);

        $this->ci['flash']->addMessage('wrong', 'Тестовый пользватель создан');
        return $response->withStatus(301)->withHeader('Location', '/login');

    }
}