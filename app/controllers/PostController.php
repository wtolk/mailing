<?php
namespace App\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Slim\Http\Request;
use Slim\Http\Response;

class PostController extends Controller
{

    public function showPublicListPosts(Request $request, $response, $args)
    {
        $this->view = 'public/posts/post-list.twig';
        $data = $request->getParams();
        $posts = Post::with('tags', 'user')
            ->where('is_published', 1)
            ->where(function ($query) use ($data) {
                $query->orWhere('title', 'like', '%'.$data['filter'].'%')
                      ->orWhere('body', 'like', '%'.$data['filter'].'%');
            })
            ->orderBy('created_at', 'desc')
            ->paginate(50);
        $this->twig_vars['posts'] = $posts;
        $this->twig_vars['filter'] = $data['filter'];
        $this->render();
    }

    public function showPublicListPostsFromCategory($request, $response, $args)
    {
        $this->view = 'public/posts/post-list.twig';
        $posts = Post::with('tags', 'user')
            ->where('category_id', $args['id'])
            ->where('is_published', 1)
            ->orderBy('created_at', 'desc')
            ->get()
            ->toArray();
        $this->twig_vars['posts'] = $posts;
        $this->render();
    }

    public function showPublicListPostsFromTag($request, $response, $args)
    {
        $this->view = 'public/posts/post-list.twig';
        $posts = Post::with('tags', 'user')
            ->whereHas('tags', function ($query) use ($args) {
                $query->where('tag_id', $args['id']);
            })
            ->where('is_published', 1)
            ->orderBy('created_at', 'desc')
            ->get()
            ->toArray();
        $this->twig_vars['posts'] = $posts;
        $this->render();
    }


    public function showPublicPost($request,Response $response, $args)
    {
        $this->view = 'public/posts/post.twig';
        $post = Post::with('tags', 'user')
            ->find($args['id'])
            ->toArray();
        // если пост приватный и пользователь неавторизован
        if ($post['is_private'] && !$this->ci['sentinel']->check()) {
            $this->ci['flash']->addMessage('notlogin', 'Для просмотра выбранного поста необходимо авторизоваться');
            return $response->withRedirect('/login');
        }
        $this->twig_vars['post'] = $post;
        $this->render();
    }

    public function showAdminPostList($request, $response, $args)
    {
        $this->view = 'admin/posts/posts-list.twig';
        $this->twig_vars['posts'] = Post::with('category', 'user')->paginate(50);
        $this-> render();
    }

    public function showAdminPostEdit($request, $response, $args)
    {
        $this->view = 'admin/posts/post-form.twig';
        $this->twig_vars['categories'] = Category::all()->toArray();
        $this->twig_vars['tags'] = Tag::all()->toArray();
        $this->twig_vars['post'] = Post::with('tags', 'user')->find($args['id'])->toArray();
        $this->render();
    }

    public function showAdminPostAdd($request, $response, $args)
    {
        $this->twig_vars['categories'] = Category::all()->toArray();
        $this->twig_vars['tags'] = Tag::all()->toArray();
        $this->view = 'admin/posts/post-form.twig';
        $this->render();
    }

    public function createPost($request, $response, $args)
    {
        $data = $request->getParams();
        $user = $this->ci['sentinel']->check();
        $data['post']['user_id'] = $user->id;
        $parsedown = new \Parsedown();
        $post = Post::create($data['post']);
        $post->tags()->attach($data['tags']);
        return $response->withRedirect($this->ci->router->pathFor('post.showAdminPostList'));
    }

    public function updatePost($request, $response, $args)
    {
        $data = $request->getParams();
        $post = Post::find($args['id']);
        $post->update($data['post']);
        $post->tags()->sync($data['tags']);
        return $response->withRedirect($this->ci->router->pathFor('post.showAdminPostList'));
    }

    public function deletePost($request, $response, $args)
    {
        $post = Post::find($args['id']);
        $post->tags()->detach();
        $post->delete();
        return $response->withRedirect($this->ci->router->pathFor('post.showAdminPostList'));
    }

}