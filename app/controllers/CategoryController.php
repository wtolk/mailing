<?php
namespace App\Controllers;
use App\Models\Category;
use App\Models\Post;

class CategoryController extends Controller
{

    public function showAdminCategoryList($request, $response, $args)
    {
        $this->view = 'admin/categorys/categorys-list.twig';
        $this->twig_vars['categorys'] = Category::paginate(50);
        $this-> render();
    }

    public function showAdminCategoryEdit($request, $response, $args)
    {
        $this->view = 'admin/categorys/category-form.twig';
        $this->twig_vars['category'] = Category::find($args['id']);
        $this->render();
    }

    public function showAdminCategoryAdd($request, $response, $args)
    {
        $this->view = 'admin/categorys/category-form.twig';
        $this->render();
    }

    public function createCategory($request, $response, $args)
    {
        $data = $request->getParams();
        Category::create($data['category']);
        return $response->withRedirect($this->ci->router->pathFor('category.showAdminCategoryList'));

    }

    public function updateCategory($request, $response, $args)
    {
        $data = $request->getParams();
        Category::find($args['id'])->update($data['category']);
        return $response->withRedirect($this->ci->router->pathFor('category.showAdminCategoryList'));
    }

    public function deleteCategory($request, $response, $args)
    {
        Category::destroy($args['id']);
        return $response->withRedirect($this->ci->router->pathFor('category.showAdminCategoryList'));
    }

}