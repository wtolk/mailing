<?php

namespace App\Controllers;

use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{
    public function showLoginPage($request, $response, $args)
    {
        $user = $this->ci['sentinel']->check();
        if($user && $this->ci['sentinel']->inRole('admin')) {
            return $response->withStatus(301)->withHeader('Location', '/admin');
        } else {
            $messages = $this->ci['flash']->getMessages();

            $this->view = 'public/users/login.twig';
            $this->twig_vars['messages'] = $messages;
            return $this->render();
        }
    }

    public function doLogin($request, $response, $args)
    {
        $data = $request->getParams();
        $credentials = [
            'email'    => $data['login'],
            'password' => $data['password'],
        ];

        $user = $this->ci['sentinel']->authenticate($credentials);

        if ($user) {
            return $response->withStatus(301)->withHeader('Location', '/admin/users');
        } else {
            $this->ci['flash']->addMessage('wrong', 'Неверные данные');
            return $response->withStatus(301)->withHeader('Location', '/login');
        }
    }

    public function doLogout($request, $response, $args)
    {
        $this->ci['sentinel']->logout();

        return $response->withStatus(301)->withHeader('Location', '/login');
    }

    public function noAccess($request, $response, $args)
    {
        $this->view = 'public/errors/message-no-access.twig';
        return $this->render();
    }

    public function showAdminUserList($request, $response, $args)
    {
        $users = User::with('role')->get()->toArray();
        $this->view = 'admin/users/users-list.twig';
        $this->twig_vars['users'] = $users;
        return $this->render();
    }

    public function showAdminUserEdit($request, $response, $args)
    {
        $this->twig_vars['roles'] = Role::orderBy('id', 'desc')->get();
        $this->twig_vars['user'] = User::with('role')->find($args['id']);
        $this->view = 'admin/users/user-form.twig';
        return $this->render();
    }

    public function showAdminUserAdd($request, $response, $args)
    {
        $this->twig_vars['roles'] = Role::orderBy('id', 'desc')->get();;
        $this->view = 'admin/users/user-form.twig';
        return $this->render();
    }

    public function createUser($request, $response, $args)
    {
        $data = $request->getParams();
        $r = $data['role'];
        unset($data['role']);

        $photo = $this->_uploadFiles('photo')[0];
        $data['photo_id'] = $photo['id'];
        $user = $this->ci['sentinel']->registerAndActivate($data);
        $role = $this->ci['sentinel']->findRoleBySlug($r);
        $role->users()->attach($user);
        return $response->withStatus(301)->withHeader('Location', '/admin/users');
    }

    public function updateUser($request, $response, $args)
    {
        $data = $request->getParams();
        if ($_FILES['photo']['size'] > 0) {
            $photo = $this->_uploadFiles('photo')[0];
            $data['photo_id'] = $photo['id'];
        }
        //Если пароль пустой, то не нужно обновлять информацию о нем
        if (strlen($data['password']) == 0) {
            unset($data['password']);
        }
        $user = $this->ci['sentinel']->findById($args['id']);
        $this->ci['sentinel']->update($user, $data);

        $user = User::with('role')->find($args['id']);

        $prevRole = $user['role'][0]['id'];
        $prevRole = $this->ci['sentinel']->findRoleById($prevRole);
        if (!is_null($prevRole)) {
            $prevRole->users()->detach($user);
        }

        $r = $data['role'];
        $role = $this->ci['sentinel']->findRoleBySlug($r);
        $role->users()->attach($user);

        return $response->withStatus(301)->withHeader('Location', '/admin/users');
    }

    public function deleteUser($request, $response, $args) {
        if ($args['id'] == $_SESSION['user']['id']) {
            throw new \Exception('Вы не можете удалить свою учетную запись<br><a href="/admin/users">Вернуться назад</a>');
        }

        $user = User::find($args['id']);

        if ($user) {
            $user->role()->detach();
            unlink($user->photo->path);
            $user->delete();
        }
        return $response->withStatus(301)->withHeader('Location', '/admin/users');
    }
}