<?php
/**
 * Created by PhpStorm.
 * User: h
 * Date: 30.08.2018
 * Time: 17:23
 */

namespace App\Controllers;


use App\Models\Tag;
use Slim\Http\Response;

class TagController extends Controller
{

    public function createTag($request,Response $response, $args)
    {
        $data = $request->getParams();
        $tag = Tag::create($data);
        return $response->write($tag->id);
    }

}