<?php
namespace App\Controllers;
use App\Models\${class_name};

class ${class_name}Controller extends Controller
{
    public function showAdmin${class_name}List($request, $response, $args)
    {
        $this->view = 'admin/${view_name}s/${view_name}s-list.twig';
        $this->twig_vars['${view_name}s'] = ${class_name}::paginate(50);
        $this-> render();
    }

    public function showAdmin${class_name}Edit($request, $response, $args)
    {
        $this->view = 'admin/${view_name}s/${view_name}-form.twig';
        $this->twig_vars['${view_name}'] = ${class_name}::find($args['id']);
        $this->render();
    }

    public function showAdmin${class_name}Add($request, $response, $args)
    {
        $this->view = 'admin/${view_name}s/${view_name}-form.twig';
        $this->render();
    }

    public function create${class_name}($request, $response, $args)
    {
        $data = $request->getParams();
        ${class_name}::create($data['${view_name}']);
        return $response->withRedirect($this->ci->router->pathFor('${view_name}.showAdmin${class_name}List'));

    }

    public function update${class_name}($request, $response, $args)
    {
        $data = $request->getParams();
        ${class_name}::find($args['id'])->update($data['${view_name}']);
        return $response->withRedirect($this->ci->router->pathFor('${view_name}.showAdmin${class_name}List'));
    }

    public function delete${class_name}($request, $response, $args)
    {
        ${class_name}::destroy($args['id']);
        return $response->withRedirect($this->ci->router->pathFor('${view_name}.showAdmin${class_name}List'));
    }

}