{% extends "admin/layout/layout.twig" %}

{% block title %}{% endblock %}

{% block content %}
<div id="page-content-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <h1 class="inline project-name">//-//-//</h1>
                <div class="inline"> <a href="{{ path_for('${class_name_low}.showAdmin${class_name}List') }}">все //-//-//</a> </div>
                <hr class="aky">
            </div>
        </div>
        <form
            method="post"
            name="login_form"
            {% if ${class_name_low}.id %}
            action="{{ path_for('${class_name_low}.update${class_name}', {'id': ${class_name_low}.id}) }}"
            {% else %}
            action="{{ path_for('${class_name_low}.create${class_name}') }}"
            {% endif %}
        >
            <div class="row">

                <div class="col-md-12">

                    <div class="form">

                        <div class="col-md-4">