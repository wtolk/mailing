
                </div>

            </div>

        </div>

    </div>
    <div class="row" style="margin-top: 15px">
        <div class="col-md-4">
            <button class="blue-button inline" type="submit">Сохранить</button>
        </div>

        <div class="col-md-4">
            {% if ${class_name_low}.id %}
                <button id="delete-user" class="red-button" type="button">Удалить пользователя</button>
            {% endif %}
        </div>
    </div>

    </form>

    <form id="frm-del-user" action="{{ path_for('${class_name_low}.delete${class_name}', {'id': ${class_name_low}.id}) }}" method="get">
    </form>

    </div>
</div>
{% endblock %}

{% block script %}

<script type="text/javascript">

    $(function() {
        $('#delete-user').click(function() {
            if (confirm('Удалить?')) {
                $('#frm-del-user').submit();
            }
        });
    });

    $(function() {
        var getFlatpickr = function() {
            return $('.flatpickr').flatpickr({
                locale: "ru",
                altFormat: 'j F Y',
                altInput: true
            });
        }

        var flatpickr = getFlatpickr();

        $('#mcm').on('shown.bs.modal', function () {
            var date = $('.flatpickr').val();
            flatpickr.destroy();
            flatpickr = getFlatpickr();
            flatpickr.setDate(date);
        });
    });
</script>
{% endblock %}