</tr>
{% set i = i+1 %}
{% endfor %}
</tbody>
</table>
</div>

</div>
</div>
<!-- /#page-content-wrapper -->
{% endblock %}

{% block script %}
<script>
    $('tbody>tr').on("mousedown", function (e) {
        if( e.which == 1 ) {
            window.location.href = $(this).data('href');
        }else if(e.which == 2){
            window.open($(this).data('href'), '_blank');
        }
    });
</script>
{% endblock %}