$( document ).ready(function() {
    window.sliders = [];
    var col = document.getElementsByClassName('slider-tarif');
    for (var i = 0; i < col.length; i++) {
        window.sliders.push(col[i]);
        noUiSlider.create(col[i], {
            start: [col[i].getAttribute('data-min'), col[i].getAttribute('data-max')],
            connect: true,
            tooltips: true,
            step: 1,
            range: {
                'min': 0,
                'max': 30
            }
        });
        console.log();
        // alert( elements[i].innerHTML ); // "тест", "пройден"
    }

    $('body').on('click', 'a.remove-slider', function () {
        // alert('click!');
        var id = $(this).attr('data-id');
        $("[data-targ='"+id+"']").remove();
        var counter = $('.add').attr('data-counter');
        $('.add').attr('data-counter', ( Number(counter) - 1 ));
    });

    $('form.add-rest').on('submit', function (event) {
        event.preventDefault();
        //считываем тарифы
        set_tariffs();
        $(this).unbind('submit').submit();

    });

    function set_tariffs() {
        var list = $( ".slider-tarif" );
        for (var i = 0; i < list.length; i++) {
            var sl = list[i];
            var diap = sl.noUiSlider.get();
            $("input[name='tarif["+i+"][rasst_min]']").val(diap[0]);
            $("input[name='tarif["+i+"][rasst_max]']").val(diap[1]);
        }
    }


    $('input.blockuser').on('click', function () {
        var id = $(this).attr('data-user-id');
        if ($(this).is(":checked"))
        {
            $.get('/api/user/block/'+id, function (data) {console.log('1');});
        } else {
            $.get('/api/user/unblock/'+id, function (data) {console.log('0');});
        }
    });



    $('.add').on('click', function () {
        var counter = $(this).attr('data-counter');
        counter = Number(counter)+1;
        $(this).attr('data-counter', counter);
        $(this).before(
            '<div data-targ="rasst'+counter+'">'+
            '<div class="col-md-8 slider-tarif" id="rasst'+counter+'"></div>' + '<input name="tarif['+counter+'][price]" class="col-md-2 price">' +
            '<input name="tarif['+counter+'][free_price_order]" class="col-md-2 free_price_order">' +
            '<input type="hidden" name="tarif['+counter+'][rasst_min]" value="">' +
            '<input type="hidden" name="tarif['+counter+'][rasst_max]" value="">' + '<a data-id="rasst'+counter+'" class="remove-slider">Удалить </a>' +
            '<div class="clear"></div>'+
            '</div>'
        );

        var slider = document.getElementById('rasst'+counter);
        window.sliders.push(slider);
        // console.log('rasst'+ ( Number(counter)-1));
        var prevslider = document.getElementById('rasst'+ ( Number(counter)-1) );
        var diap = prevslider.noUiSlider.get();
        var min = diap[1];
        var max = Number(diap[1])+30.00;
        max = parseFloat(max).toFixed(2);
        noUiSlider.create(slider, {
            start: [Number(min), Number(min)+20],
            connect: true,
            tooltips: true,
            step: 1,
            // range: {
            //     'min': Number(min),
            //     'max': Number(max)
            // }
            range: {
                'min': 0,
                'max': 30
            }
        });
        getSlidersEvents();
    });


    getSlidersEvents();
function getSlidersEvents() {
    var list = $( ".slider-tarif" );
    for (var i = 0; i < list.length; i++) {
        var slider = list[i];
        slider.noUiSlider.on('change', function ( values, handle ) {
            var number = this.target.id.substr(5);
            var count = $( ".slider-tarif" ).length;
            var curslider = document.getElementById('rasst'+number);
            var diap = curslider.noUiSlider.get();
            var max = Number(diap[1]);
            if (number != 0) {
                var prevslider = document.getElementById('rasst'+(number-1));
                var prevdiap = prevslider.noUiSlider.get();
                var min = Number(prevdiap[1]);
                if ( values[handle] < min ) {
                    curslider.noUiSlider.set(min);
                }
            }

            if (number != (count - 1)) {
                var nextslider = document.getElementById('rasst'+(Number(number)+1));
                var nextdiap = nextslider.noUiSlider.get();
                var nextmin = Number(nextdiap[0]);
                if ( max > nextmin ) {
                    nextslider.noUiSlider.set(max);
                }
            }

        });
    }
}


    $('input.restaurant-address').autoComplete({
        source: function(term, response){
            $.getJSON('/api/order/autocomplete', { phrase: term }, function(data){
                response(data);
            });
        },
        renderItem: function (item, search){
            return '<div class="autocomplete-suggestion" data-val="'+search+'">'+item.addr_city+', '+item.addr_street+', '+item.addr_house+'</div>';
        },
        onSelect: function(e, term, item){
            $("input.restaurant-address").val(item[0].innerText);
        }
    });




$('#filter-status').on('change', function () {
    var status = $( "#filter-status option:selected" ).val();
    console.log(status);
    if (status == 'all') {
        if($("#filter-status").hasClass("partner")) {
            $('a#filter-link').attr('href', '/admin/partner/orders');
        } else {
            $('a#filter-link').attr('href', '/admin/orders');
        }
    } else {
        console.log(status);
        $('a#filter-link').attr('href', '?status='+status);
    }
})


    
});