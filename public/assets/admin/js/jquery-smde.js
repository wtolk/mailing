/**
 * <%= pkg.name %>
 * Simple markdown editor and parser plugin for jQuery.
 *
 * Copyright (c) 2015-2016 akeon GmbH, Marius Friedrichs
 *
 * Version: <%= pkg.version %> (<%= grunt.template.today("yyyy-mm-dd") %>)
 * License: <%= pkg.license %>
 */
;// jshint ignore:line
(function(factory) {// jshint ignore:line
    "use strict";

    /* jshint ignore:start */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery', undefined], factory);

    } else {
        // Browser globals
        factory(jQuery, undefined);
    }
    /* jshint ignore:end */

})(function($, undefined) {
    "use strict";

    /**
     * Parses markdown and returns HTML string.
     *
     * @param {string} markdown
     *
     * @return {string}
     */
    var parseMarkdown = function(markdown) {
        /**
         * @param {string} string
         */
        function escapeChars(string) {
            string = string
                .replace(/ \* /g, ' !ast! ')
                .replace(/ _ /g, ' !us! ')

                // backslash escapes
                .replace(/\\(\\|`|\*|_|{|}|\[|]|\(|\)|#|\+|-|\.|!)/gim, function(match, char) {
                    return '&#' + char.charCodeAt(0) + ';';
                });

            return string;
        }


        /**
         * @param {string} string
         */
        function unescapeChars(string) {
            string = string
                .replace(/!ast!/g, '*')
                .replace(/!us!/g, '_');

            return string;
        }


        /**
         * Parses text between backticks "`" and encodes special
         * chars to unicode entities.
         *
         * @param {string} string
         */
        function parseCode(string) {
            return string.replace(/(``(?:.|\n)+?``|`(?:.|\n)+?`)/g, function(match, code) {
                var tag           = 'code',
                    encodedString = code.replace(/[\u00A0-\u9999<>&]/gim, function(char) {
                        return '&#' + char.charCodeAt(0) + ';';
                    });

                encodedString = encodedString.substr(1, encodedString.length - 2);

                if (0 === encodedString.indexOf('`')) {
                    encodedString = encodedString.substr(1, encodedString.length - 2);
                }

                if (-1 !== encodedString.indexOf('\n')) {
                    tag = 'pre';

                    encodedString = encodedString.replace(/\n\n/g, '#!nn!#');
                    encodedString = encodedString.replace(/\t/gm, '    ');
                }

                return '<' + tag + '>' + encodedString + '</' + tag + '>';
            });
        }


        /**
         * Recursively parses string for (un-)ordered lists.
         *
         * @param {string} string
         */
        function parseLists(string) {
            return string.replace(
                /(?:^|\n)\n?((?:(\d\.|-|\*|\+) +.+(\n+(?: {4}|\t).+)*(?:\n{1,2}|$))+?)(?:\n{1,2}|$)/gm,
                function(match) {
                    match = match.replace(/\n\n/gm, '\n');

                    var type      = null,
                        listItems = match.replace(
                            /^(?:\d\.|-|\*|\+) +(.+((\n+(?: {4}|\t)(\d\.|-|\*|\+)?.+)*))(?:\n{1,2}|$)/gm,
                            function(match, innerHtml, $2) {
                                if (null === type) {
                                    type = match.substr(0, 1);
                                }

                                if ('undefined' !== typeof $2) {
                                    var replace = $2.split(/\n(?: {4}|\t)/);

                                    replace = $.each(replace, function() {
                                        return $.trim(this);
                                    });

                                    replace = parseLists(replace.join('\n'));

                                    innerHtml = innerHtml.replace($2, replace);
                                }

                                return '<li><p>' + $.trim(innerHtml) + '</p></li>';
                            });

                    listItems = listItems.replace('/>\n</g', '');

                    if (type == parseInt(type)) {
                        return '\n<ol>' + listItems + '</ol>\n';

                    } else {
                        return '\n<ul>' + listItems + '</ul>\n';
                    }
                });
        }


        /**
         * Recursively parses string for blockquotes.
         *
         * @param {string} string
         */
        function parseQuotes(string) {
            return string.replace(
                /(?:^|\n)(?:(?:>.*|>)(?:\n*|$))+/gim,
                function(match) {
                    match     = match.split('\n');
                    var quote = [];

                    $.each(match, function(index, value) {
                        var line = $.trim(value);

                        if ('' === line) {
                            line = '\n';
                        }

                        if ('>' === line.substr(0, 1)) {
                            line = line.substr(2);
                        }

                        quote.push(line);
                    });

                    quote = quote.join('\n');

                    return '<blockquote>\n' + parseQuotes($.trim(quote)) + '\n</blockquote>\n';
                }
            );
        }

        // normalize line endings
        markdown = markdown
            .replace(/\r\n|\r/gi, '\n');

        // escape chars
        markdown = escapeChars(markdown);

        // quotes
        markdown = parseQuotes(markdown);

        // parse lists
        markdown = parseLists(markdown);

        markdown = markdown

        // headings
            .replace(/(?:^|\n)(#{1,6})(.+?)(?:\n|$)/gi,
                function(match, level, heading) {
                    return '<h' + level.length + '>' + $.trim(heading) + '</h' + level.length + '>';
                })

            // alternate headings
            .replace(/(?:(?:^|\n)(?!\+|\*|#|>|-))(.+)\n(=+|-+)(?:\n|$)/gi,
                function(match, heading, $2) {
                    if (heading.length === $2.length) {
                        var type  = $2.substr(0, 1),
                            level = 1;

                        if ('-' === type) {
                            level = 2;
                        }

                        return '<h' + level + '>' + $.trim(heading) + '</h' + level + '>';

                    } else {
                        return match;
                    }
                })

            // bold italic
            .replace(/\*\*\*(.+?)\*\*\*/gim, '<strong><em>$1</em></strong>')
            .replace(/___(.+?)___/gim, '<strong><em>$1</em></strong>')

            // bold
            .replace(/\*\*(.+?)\*\*/gim, '<strong>$1</strong>')
            .replace(/__(.+?)__/gim, '<strong>$1</strong>')

            // italic
            .replace(/_(.+?)_/gim, '<em>$1</em>')
            .replace(/\*(.+?)\*/gim, '<em>$1</em>')

            // strikethrough
            .replace(/~~(.+?)~~/gim, '<s>$1</s>')

            // link
            .replace(
                /(?:\[(.[^\n\t]+?)])?\((https?:\/\/.[^\s)]+?)(?: "(.[^\n\t)]+?)")?\)|<(https?:\/\/.[^\s)]+)>/gim,
                function(match, description, href, tooltip, simpleLink) {
                    if ('undefined' !== typeof simpleLink) {
                        return '<a href="' + simpleLink + '" target="_blank" title="' + simpleLink + '">' +
                               simpleLink +
                               '</a>';

                    } else {
                        // tooltip
                        if ('undefined' === typeof tooltip) {
                            tooltip = href;
                        }

                        return '<a href="' + href + '" target="_blank" title="' + tooltip + '">' + description + '</a>';
                    }
                })

            // link (email)
            .replace(
                /<(.+@.+\..+)>/gim,
                function(match, email) {
                    email = email.replace(/[\u0000-\u9999<>&]/gim, function(i) {
                        return '&#' + i.charCodeAt(0) + ';';
                    });

                    return '<a href="mailto:' + email + '" title="' + email + '">' + email + '</a>';
                });

        // code
        markdown = parseCode(markdown);

        // html line endings
        markdown = markdown
            .replace(/(?:\n\n)/g, '<br/><br/>')
            .replace(/(?: \n)/g, '<br/>')
            .replace(/<br\/><([hou])/gm, '<$1')

            // line endings in "code"
            .replace(/#!nn!#/g, '\n\n');

        // unescape chars
        markdown = unescapeChars(markdown);

        return markdown;
    };


    /**
     * SimpleMarkdownEditor constructor function.
     *
     * @param {object} element
     * @param {object} options
     *
     * @constructor
     */
    function SimpleMarkdownEditor(element, options) {
        if (!(this instanceof SimpleMarkdownEditor)) {
            return new SimpleMarkdownEditor(element, options);
        }

        this.$element = $(element);
        this.element  = element;
        this.editor   = {};
        this.options  = $.extend({}, $.fn.SimpleMarkdownEditor.defaults, options);

        this.mdOptions = {
            boldItalic:    ['***', '***', this.translate('bold-italic text')],
            bold:          ['**', '**', this.translate('bold text')],
            italic:        ['_', '_', this.translate('italic text')],
            link:          ['[' + this.translate('link description') + '](', ')', this.translate('link')],
            orderedList:   ['\n1. ', '', this.translate('list item')],
            unorderedList: ['\n- ', '', this.translate('list item')],
            code:          ['`', '`', this.translate('code')],
            quote:         ['\n> ', '\n\n', this.translate('quoted text')]
        };

        this.init();
    }


    /**
     * Attaches appropriate click event to the given button
     * depending on the given name.
     *
     * @param {object} button
     * @param {string} name
     *
     * @returns {*}
     */
    SimpleMarkdownEditor.prototype.attachBtnClick = function(button, name) {
        var me = this;

        switch (name) {
            case 'updatePreview':
                if (false === me.options.preview) {
                    button.addClass('disabled');
                }

                button
                    .click(function() {
                        var $button = $(this);

                        if (!$button.hasClass('disabled')) {
                            var $preview = me.editor.next();

                            if (true === me.options.sideBySide) {
                                $preview = $preview.find('.md-preview');
                            }

                            if ('object' === typeof $preview && $preview.hasClass('md-preview')) {
                                $preview.html(parseMarkdown(me.$element.val()));

                                if (false === me.options.sideBySide) {
                                    $(me.options.icons.close)
                                        .attr('title', me.translate('Click to close'))
                                        .click(function() {
                                            $('#' + me.element.id)
                                                .parent().find('.md-preview').trigger('click');
                                        })
                                        .prependTo($preview);
                                }

                            } else if ('' === me.$element.val()) {
                                $preview.fadeOut('fast', function() {
                                    $(this).remove();
                                });
                            }

                            $button.blur();
                        }
                    });
                break;

            case 'preview':
                button.click(function() {
                    var $preview = me.editor.next();

                    if ('object' === typeof $preview && $preview.hasClass('md-preview')) {
                        me.editor.find('.md-updatePreview').addClass('disabled');
                        $preview.fadeOut('fast', function() {
                            $(this).remove();
                        });

                    } else if ('' !== me.$element.val()) {
                        me.showPreview();
                    }

                    $(this).blur();
                });
                break;

            default:
                button.click(function() {
                    var selection = me.$element.textrange('get', 'text');

                    if ('' === selection) {
                        selection = me.mdOptions[name][2];
                        me.$element.textrange('setcursor', me.$element.val().length);
                    }

                    var markdown = me.mdOptions[name][0].toString() + selection + me.mdOptions[name][1].toString();

                    me.$element.textrange('replace', markdown);

                    // select only placeholder text, not the inserted markdown
                    var selectionPos = me.$element.textrange('get');

                    me.$element.textrange(
                        'set',
                        selectionPos.start + me.mdOptions[name][0].length,
                        selectionPos.length - me.mdOptions[name][0].length - me.mdOptions[name][1].length
                    );
                });
        }

        return button;
    };


    /**
     * Initializes the editor.
     */
    SimpleMarkdownEditor.prototype.init = function() {
        if (true === this.options.editor) {
            // wrap editor panel around textarea
            this.$element.wrap('<div class="md-editor panel panel-default"></div>');
            this.editor = this.$element.parent();


            if (true === this.options.hideEditor) {
                this.options.sideBySide = false;

                this.editor.addClass('hidden');

            } else if (true === this.options.sideBySide) {
                this.options.preview         = true;
                this.options.buttons.preview = false;

                this.editor.wrap('<div class="col-md-6"></div>')
                    .parent().wrap('<div class="row"></div>');

                this.editor = this.editor.parent();
            }

            this.insertMenu();
        }

        if (true === this.options.preview) {
            this.showPreview();

            if (false === this.options.editor) {
                this.$element.remove();
            }
        }
    };


    /**
     * Inserts the menu toolbar.
     */
    SimpleMarkdownEditor.prototype.insertMenu = function() {
        var me          = this,
            $menu       = $('<div class="md-toolbar panel-heading"></div>'),
            buttons     = me.options.buttons,
            buttonsTips = {
                bold:          'Bold',
                boldItalic:    'Bold/Italic',
                code:          'Code',
                italic:        'Italic',
                link:          'Link',
                orderedList:   'Ordered list',
                preview:       'Toggle preview',
                quote:         'Quote',
                updatePreview: 'Update preview',
                unorderedList: 'Unordered list'
            };

        if (false === $.isEmptyObject(buttons)) {
            var buttonGroups = {
                1: ['bold', 'italic', 'boldItalic'],
                2: ['link', 'unorderedList', 'orderedList'],
                3: ['code', 'quote'],
                4: ['preview', 'updatePreview']
            };

            $.each(buttonGroups, function(idx, group) {
                var $currGroup = $('<div class="btn-group btn-group-sm clearfix"></div>');

                $.each(group, function(idx, name) {
                    if (true === buttons[name]) {
                        var $button = $('<button type="button" class="btn btn-default md-' +
                                        name + '" title="' + me.translate(buttonsTips[name]) +
                                        '" />')
                            .append(me.options.icons[name]);

                        $button = me.attachBtnClick($button, name);

                        $currGroup.append($button);
                    }
                });

                if (0 < $currGroup.children.length) {
                    $menu.append($currGroup);
                    $menu.append('<div class="clearfix visible-xs-block"></div>');
                }
            });

            if (0 < $menu.children.length) {
                me.$element.before($menu);
            }
        }
    };


    /**
     * Inserts the preview panel.
     */
    SimpleMarkdownEditor.prototype.showPreview = function() {
        var me       = this,
            html     = parseMarkdown(me.$element.val()),
            $preview = $('<article class="md-preview well"></article>');

        $preview.append(html);

        if (true === me.options.editor) {
            if (true === this.options.hideEditor) {
                $(me.options.icons.edit)
                    .attr('title', me.translate('Click to edit'))
                    .click(function() {
                        $('#' + me.element.id).parent().removeClass('hidden')
                            .end().focus();

                        $(this).parent().remove();
                    })
                    .prependTo($preview);

            } else if (false === this.options.sideBySide) {
                $(me.options.icons.close)
                    .attr('title', me.translate('Click to close'))
                    .click(function() {
                        $('#' + me.element.id).parent().find('.md-preview').trigger('click');
                    })
                    .prependTo($preview);
            }

            if (true === this.options.sideBySide) {
                $preview.wrap('<div class="col-md-6"></div>');

                $preview = $preview.parent();
            }

            me.editor
                .after($preview)
                .find('.md-updatePreview').removeClass('disabled');

        } else {
            me.$element.after($preview);
        }
    };


    /**
     * Localization function.
     *
     * @param {string} string
     * @returns {string} string
     */
    SimpleMarkdownEditor.prototype.translate = function(string) {
        var l10n     = $.fn.SimpleMarkdownEditor.l10n,
            language = this.options.locale;

        if ('undefined' !== typeof l10n &&
            'undefined' !== typeof l10n[language] &&
            'undefined' !== typeof l10n[language][string]
        ) {
            return l10n[language][string];
        }

        return string;
    };


    /**
     * Lightweight plugin wrapper to prevent multiple instantiations.
     *
     * @param {object} options
     *
     * @returns {*}
     *
     * @constructor
     */
    $.fn.SimpleMarkdownEditor = function(options) {
        return this.each(function() {
            if (!$.data(this, 'plugin_SimpleMarkdownEditor')) {
                $.data(this, 'plugin_SimpleMarkdownEditor',
                    new SimpleMarkdownEditor(this, options));
            }
        });
    };


    /**
     * Default settings.
     */
    $.fn.SimpleMarkdownEditor.defaults = {
        editor:     true,
        preview:    false,
        sideBySide: false,
        hideEditor: false,
        locale:     'en',
        buttons:    {
            bold:          true,
            italic:        true,
            boldItalic:    true,
            link:          true,
            orderedList:   true,
            unorderedList: true,
            code:          true,
            preview:       true,
            updatePreview: true,
            quote:         true
        },
        icons:      {
            bold:          '<span class="glyphicon glyphicon-bold"></span>',
            boldItalic:    '<span class="glyphicon glyphicon-bold"></span>' +
                           '<span class="glyphicon glyphicon-italic"></span>',
            close:         '<span class="glyphicon glyphicon-remove pull-right"></span>',
            code:          '<span class="glyphicon glyphicon-asterisk"></span>',
            edit:          '<span class="glyphicon glyphicon-edit pull-right"></span>',
            italic:        '<span class="glyphicon glyphicon-italic"></span>',
            link:          '<span class="glyphicon glyphicon-link"></span>',
            orderedList:   '<span class="glyphicon glyphicon-list"></span>',
            preview:       '<span class="glyphicon glyphicon-search"></span>',
            updatePreview: '<span class="glyphicon glyphicon-refresh"></span>',
            quote:         '<span class="glyphicon glyphicon-comment"></span>',
            unorderedList: '<span class="glyphicon glyphicon-th-list"></span>'
        }
    };


    /**
     * Placeholder for localization object.
     */
    $.fn.SimpleMarkdownEditor.l10n = {};


    /**
     * Parses markdown on the given elements.
     *
     * @param {bool} preserveWhitespace
     * @param {bool} trimContent
     *
     * @returns {*}
     */
    $.fn.parseMarkdown = function(preserveWhitespace, trimContent) {
        return this.each(function() {
            $.parseMarkdown(this, preserveWhitespace, trimContent);
        });
    };


    /**
     * Parses markdown on the given selector
     * (defaults to '[data-parse-markdown="true"]').
     *
     * @param {object} selector
     * @param {bool} preserveWhitespace
     * @param {bool} trimContent
     *
     * @returns {*}
     */
    $.parseMarkdown = function(selector, preserveWhitespace, trimContent) {
        if (undefined === selector) {
            selector = '[data-parse-markdown="true"]';
        }

        if (undefined === preserveWhitespace) {
            preserveWhitespace = true;
        }

        if (undefined === trimContent) {
            trimContent = true;
        }

        $(selector).each(function() {
            var me       = this,
                markdown = me.innerHTML;

            if (true === trimContent) {
                markdown = $.trim(markdown);
            }

            if (false === preserveWhitespace) {
                markdown = markdown.replace(/\n(?: |\t)*/gim, '\n');
            }

            me.innerHTML = parseMarkdown(markdown);
        });

        return this;
    };
});
